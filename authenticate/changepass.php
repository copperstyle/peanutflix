<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>PeanutFlix - Alteração de palavra-passe</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../peanuttime.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href='../index.php'>
            <img src="../assets/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
            PeanutFlix
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filmes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="../movies.php">All</a>
                        <a class="dropdown-item" href="../movies_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Séries
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="../tv.php">All</a>
                        <a class="dropdown-item" href="../tv_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-nav float-md-right">
                <li class="nav-item">
                    <button type="button" onclick="location.href='login.php'" class="btn btn-outline-success">Login / Sign In</button>
                </li>
            </ul>
        </div>
    </nav>
</head>

<?php

include('config.php');


$token = $_GET['token'];

if($token == null || strlen($token)!=100){ //NÃO PERMITE AO UTILIZADOR ENTRAR NA PÁGINA SEM TOKEN DE ACESSO
    echo ("<script LANGUAGE='JavaScript'>
                                window.alert('Não tem autorização para aceder a esta página!');
                                window.location.href='../index.php';
                                </script>");
}

$sql = "SELECT * FROM utilizadores WHERE token = '$token' LIMIT 1";

$result = $conn->query($sql);

$erro_registo_password = "";
if (!$result) {
    trigger_error('Invalid query: ' . $conn->error);
} else {
    if ($result->num_rows != 0) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $email = $row['email'];
        if (isset($_POST['confPass'])) {
            $new_pass = $_POST['new_password'];
            $conf_pass = $_POST['conf_password'];
            if ($new_pass == $conf_pass) {
                //-------AS PALAVRAS-PASSE COINCIDEM
                if (empty($new_pass)) { //check if string is empty
                    $erro_registo_password = "Insira dados no campo password";
                } else if (strlen($new_pass) <= 7) {
                    $erro_registo_password = "A password deve conter no mínimo 7 carateres";
                } else if (!preg_match("#[0-9]+#", $new_pass)) {
                    $erro_registo_password = "A password deve conter no mínimo um número";
                } else if (!preg_match("#[a-z]+#", $new_pass)) {
                    $erro_registo_password = "A password deve conter no mínimo uma letra minúscula";
                } else if (!preg_match("#[A-Z]+#", $new_pass)) {
                    $erro_registo_password = "A password deve conter no mínimo uma letra maiúscula";
                } else {
                    $new_pass = password_hash(filter_var($new_pass, FILTER_SANITIZE_STRING), PASSWORD_BCRYPT);
                    $sql2 = "UPDATE utilizadores SET password='$new_pass', token=NULL WHERE email='$email'";
                    if ($conn->query($sql2) === TRUE) {
                        //MOSTRA JANELA DE AVISO A CONFIRMAR A ALTERAÇÃO E RETORNA À PÁGINA DE INÍCIO
                        echo ("<script LANGUAGE='JavaScript'>
                                window.alert('Palavra-passe alterada com sucesso!');
                                window.location.href='../index.php';
                                </script>");
                    } else {
                        echo "Error: " . $sql2 . "<br>" . $conn->error;
                    }
                }
            } else {
                //-------AS PALAVRAS-PASSE NÃO COINCIDEM
                if (empty($new_pass)) {
                    $erro_registo_password = 'um dos campos está vazio';
                } else {
                    $erro_registo_password = 'as passes n coincidem';
                }
            }
        }
    }
}

?>

<body class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6"  style="margin: 0 auto;">
                <form class="formulario2" method="post">
                    <br>
                    <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
                    <h1 class="h3 mb-3 font-weight-normal">Recuperação de palavra-passe</h1>

                    <span class="error"> </span>

                    <input style="margin-top: 8px;" type="password" name="new_password" class="form-control" placeholder="Password">
                    
                    <input style="margin-top: 8px;margin-bottom: 8px;" type="password" name="conf_password" class="form-control" placeholder="Confirmar password">
                    <span class="error"> <?php echo $erro_registo_password; ?> </span>
                    <button id="confPass" class="btn btn-lg btn-primary btn-block" type="submit" value="Yes" name="confPass">Confirmar nova palavra-passe</button>

                </form>
            </div>
        </div>
    </div>
</body>

<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</footer>

</html>