<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>PeanutFlix</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../peanuttime.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php">
            PeanutTime
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filmes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="../movies.php">All</a>
                        <a class="dropdown-item" href="../movies_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Séries
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="../tv.php">All</a>
                        <a class="dropdown-item" href="../tv_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>

            </ul>
            <?php
            include("verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

            if ($autenticado) {
            ?>
                <ul class="nav navbar-nav float-md-right">
                    <li class="nav-item dropdown ml-auto">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="backend/perfil.php">Perfil</a>
                            <a class="dropdown-item" href="#">Aluguer de Filmes</a>
                            <a class="dropdown-item" href="#">Filmes Alugados</a>
                            <a class="dropdown-item" style="color:red;" href="logout.php">Logout</a>
                        </div>
                    </li>
                </ul>
            <?php
            } else {
            ?>
                <ul class="nav navbar-nav float-md-right">
                    <li class="nav-item">
                        <button type="button" onclick="location.href='login.php'" class="btn btn-outline-success">Login / Sign In</button>
                    </li>
                </ul>
            <?php
            }
            ?>
        </div>
    </nav>
</head>


<body>

    <div class="container">
        <div class="text-center">
            <h1 class="display-3">Thank You!</h1>
            <p class="lead">Your account has been successfully created, if u want to login please click <a style="color:
                    blue;" href="login.php">here</a> to get redirected to the login page</p>
        </div>
    </div>

</body>

<footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</footer>

</html>