<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix - Login/Registo</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link href="../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>



  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href='../index.php'>
      PeanutFlix
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filmes
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="../movies.php">All</a>
            <a class="dropdown-item" href="../movies_generos.php">Generos</a>
            <a class="dropdown-item" href="#"></a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Séries
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="../tv.php">All</a>
            <a class="dropdown-item" href="../tv_generos.php">Generos</a>
            <a class="dropdown-item" href="#"></a>
          </div>
        </li>
        

      </ul>
      <ul class="nav navbar-nav float-md-right">
        <li class="nav-item">
          <button type="button" onclick="location.href='login.php'" class="btn btn-outline-success">Login / Sign In</button>
        </li>
      </ul>

    </div>
  </nav>



</head>


<body>
  <?php

  include("config.php");

/*
  $sql = "CREATE TABLE utilizadores (
       numero INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       login VARCHAR(30) NOT NULL UNIQUE KEY,
       password VARCHAR(255) NOT NULL,
       nivel VARCHAR(30) NOT NULL DEFAULT 'Utilizador',
       email VARCHAR(30) NOT NULL UNIQUE KEY,
       token VARCHAR(255) UNIQUE KEY,
       imagem BLOB)";
  if ($conn->query($sql) === TRUE) echo "Tabela Utilizadores criada com sucesso!!!";
  else echo "Erro na criação da tabela: " . $conn->error;
*/

  $erro_sessao = "";

  if (isset($_POST['login'])) {

    if (!isset($_SESSION)) {
      session_start();
    }

    $login_email =  filter_var($_POST['login_email'], FILTER_SANITIZE_EMAIL);
    $login_password = filter_var($_POST['login_password'],FILTER_SANITIZE_STRING);

    


    $sql = "SELECT * FROM utilizadores WHERE email='$login_email'";
    $result = $conn->query($sql);

    if ($result->num_rows != 0) {
      $row = $result->fetch_array(MYSQLI_ASSOC);
      $aaa = $row['password'];
      if (password_verify($login_password, $aaa)) {

       /* if ($_POST["remember"] === '1' || $_POST["remember"] === 'on') {
          $hour = time() + 3600 * 24 * 30;
          setcookie('email', $login_email, $hour);
          print_r($_COOKIE['email']);
        }*/

        if($_POST['remember']==true){
          $hour = time() + 3600;
          setcookie('email',$login_email,$hour);
        } else {
          $hour = time() + 1;
          setcookie('email',$login_email,$hour);
        }


        $_SESSION['email'] = $login_email;
        $_SESSION['password'] = $aaa;


        //header("Location: http://saw.com/peanutflix/index.php");
        header("Location: ../index.php");
        die();
      }
    } else {
      $erro_sessao = "Dados de acesso inválidos. Por favor, tente novamente";
      session_destroy();
    }
  }


  //----------------------------------------------------------INSERÇÃO------------------------------------------------------------------------
  $erro_registo_username = "";
  $erro_registo_password = "";
  $erro_registo_email = "";

  if (isset($_POST['inserir'])) {


    $registo_username = filter_var($_POST['registo_username'], FILTER_SANITIZE_STRING);

    //$registo_password = ;
    $registo_password = filter_var($_POST['registo_password'],FILTER_SANITIZE_STRING);
    

    $registo_email =  filter_var($_POST['registo_email'], FILTER_SANITIZE_EMAIL);


    //------------------------LOGIN-------------------------------------------------------------------------------------

    $row_cnt = 0;

    if (empty($registo_username)) {
      $erro_registo_username = "Insira dados no campo de login";
    } else if ((strlen($registo_username)) <= 7) {
      $erro_registo_username = "A login necessita de um mínimo de 7 carateres";
    }

    if ($resultado = $conn->query("SELECT login FROM Utilizadores WHERE login='$registo_username'")) {
      $row_cnt = $resultado->num_rows;
      if ($row_cnt != 0) {
        $erro_registo_username = "A login inserida já existe";
      }
    }


    //------------------------PASSWORD----------------------------------------------------------------------------------

    if (empty($registo_password)) { //check if string is empty
      $erro_registo_password = "Insira dados no campo password";
    } else if (strlen($registo_password) <= 7) {
      $erro_registo_password = "A password deve conter no mínimo 7 carateres";
    } else if (!preg_match("#[0-9]+#", $registo_password)) {
      $erro_registo_password = "A password deve conter no mínimo um número";
    } else if (!preg_match("#[a-z]+#", $registo_password)) {
      $erro_registo_password = "A password deve conter no mínimo uma letra minúscula";
    } else if (!preg_match("#[A-Z]+#", $registo_password)) {
      $erro_registo_password = "A password deve conter no mínimo uma letra maiúscula";
    }

    //------------------------EMAIL-------------------------------------------------------------------------------------

    if (empty($registo_email)) { //check if string is empty
      $erro_registo_email = "Insira dados no campo de email";
    } else if (!filter_var($registo_email, FILTER_VALIDATE_EMAIL)) {
      $erro_registo_email = "Endereço de email inválido";
    }

    if ($resultado = $conn->query("SELECT email FROM Utilizadores WHERE email='$registo_email'")) {
      $row_cnt = $resultado->num_rows;
      if ($row_cnt != 0) {
        $erro_registo_email = "O email inserido já existe";
      }
    }

    //------------------------INSERÇAO----------------------------------------------------------------------------------


    if(($erro_registo_username == "") && ($erro_registo_password == "") && ($erro_registo_email == "")){ 
          $nivel="Utilizador";
          $hashed_password = password_hash($registo_password, PASSWORD_BCRYPT);

          if($stmt=$conn->prepare("INSERT INTO utilizadores(login, password, nivel, email) VALUES (?,?,?,?)"))
          {
                $stmt->bind_param("ssss", $registo_username, $hashed_password, $nivel, $registo_email);
                $stmt->execute();
                $stmt->close(); 
                
                header("Location: confirmacao.php");
                die();
          }
    }
  }
  ?>
  <!-------------------------------------------------------------FORMULARIOS ------------------------------------------------------------>

  <body class="text-center">
    <div class="container">
      <div class="row">

        <div class="col-md-5">
          <form class="formulario" method="post">
            <br>
            <h1 class="h3 mb-3 font-weight-normal">Login</h1>

            <span class="error"> <?php echo $erro_sessao; ?> </span>
            <input type="email" name="login_email" class="form-control"
             placeholder="Email address" value="<?php echo @$_COOKIE['email']; ?>">

            <input style="margin-top: 8px;margin-bottom: 8px;" type="password" name="login_password" class="form-control" placeholder="Password">
            <input style="margin-bottom: 8px;" type="checkbox" name="remember">  Lembrar utilizador</input><br>
            <a style="color:red;" href="forget.php">Esqueceu-se da palavra-passe?</a>
            <br>
            <button style="margin-top: 8px;" class="btn btn-lg btn-primary btn-block" type="submit" name="login">Log in</button>
          </form>
        </div>

        <div class="col-md-2">
        </div>

        <div class="col-md-5">
          <form class="formulario" method="post">
            <br>
            <h1 class="h3 mb-3 font-weight-normal">Register</h1>

            <input type="email" name="registo_email" class="form-control" placeholder="Email address">
            <span class="error"> <?php echo $erro_registo_email; ?> </span>

            <input style="margin-top: 8px;margin-bottom: 8px;" type="name" name="registo_username" class="form-control" placeholder="Username">
            <span class="error"> <?php echo $erro_registo_username; ?> </span>

            <input style="margin-top: 8px;" type="password" name="registo_password" class="form-control" placeholder="Password">
            <span class="error"> <?php echo $erro_registo_password; ?> </span>
            <br>
            
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="inserir">Sign in</button>
          </form>
        </div>
      </div>
    </div>
  </body>

  <footer>
    <?php
    $conn->close();
    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

  </footer>

</html>