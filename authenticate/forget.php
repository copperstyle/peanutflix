<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>PeanutFlix - Esqueceu-se da palavra-passe?</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../peanuttime.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href='../index.php'>
            <img src="../assets/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
            PeanutFlix
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filmes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="../movies.php">All</a>
                        <a class="dropdown-item" href="../movies_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Séries
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="../tv.php">All</a>
                        <a class="dropdown-item" href="../tv_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>

            </ul>
            <ul class="nav navbar-nav float-md-right">
                <li class="nav-item">
                    <button type="button" onclick="location.href='login.php'" class="btn btn-outline-success">Login / Sign In</button>
                </li>
            </ul>

        </div>
    </nav>

</head>

<?php


include("config.php");

$erro_registo_email = "";

if (isset($_POST['reqEmail'])) {

    $email = filter_var($_POST['login_email'], FILTER_SANITIZE_EMAIL);

    $sql = "SELECT * FROM utilizadores WHERE email='$email' LIMIT 1";
    $result = $conn->query($sql);

    if ($result->num_rows != 0) {

        $token = bin2hex(random_bytes(50));  //CRIA UM TOKEN

        $sql2 = "UPDATE utilizadores SET token='$token' WHERE email='$email'"; //INSERE O TOKEN NA CONTA

        if ($conn->query($sql2) === TRUE) {
            //ENVIAR E-MAIL
            $to = $email;
            $subject = 'Peanutflix - Recuperação de palavra-passe';
            $headers = 'From: Peanutflix Admin' . "\r\n";
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            $urls = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            $search = "forget.php";
            $trim = str_replace($search, "changepass.php?token=", $urls);
            echo $trim;
            $link = "http://".$trim.$token;
            $message = "<p>Olá, carregue neste <a href=\""
                . $link . "\">link</a> para proceder à alteração da palavra-passe.</p>
        <p> </p><p>Cumprimentos,</p><p>PeanutFlix</p>";


            if (mail($to, $subject, $message, $headers)) {
                header("Location: confirm.php");
            }
        } else {
            echo "Error: " . $sql2 . "<br>" . $conn->error;
        }
    } else {
        $erro_registo_email = "Não existe conta com este e-mail";
    }
}



//------------------------------------------Erros-----------------------------------------------------------



$conn->close();
?>

<body class="text-center">


    <div class="container">
        <div class="row">
            <div class="col-md-6" style="margin: 0 auto;">
                <form class="formulario2" method="post">
                    <br>
                    
                    <h1 class="h3 mb-3 font-weight-normal">Recuperação de palavra-passe</h1>

                    <span class="error"> </span>
                    <input id="fieldEmail" type="email" name="login_email" class="form-control" placeholder="Endereço e-mail">
                    <span class="error"> <?php echo $erro_registo_email; ?> </span>
                    <button style="margin-top: 8px" id="btnEmail" class="btn btn-lg btn-primary btn-block" type="submit" value="Yes" name="reqEmail">Confirmar e-mail</button>

                </form>
            </div>
        </div>
    </div>



</body>


<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</footer>

</html>