<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="./assets/img/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">
      PeanutFlix
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filmes
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="frontend/movies.php">All</a>
            <a class="dropdown-item" href="frontend/movies_generos.php">Generos</a>
            <a class="dropdown-item" href="#"></a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Séries
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="frontend/tv.php">All</a>
            <a class="dropdown-item" href="frontend/tv_generos.php">Generos</a>
            <a class="dropdown-item" href="#"></a>
          </div>
        </li>
      </ul>
      <?php
      include("authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

      if ($autenticado) {
        if ($cargoUtil == "Utilizador") {
      ?>
          <ul style="margin-top: 0.5%; float: right;">
            <li class="nav-item dropdown ml-auto">
              <a class="nav-link dropdown-toggle" href="#" style="color: grey !important;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="backend/user/perfil.php">Perfil</a>
                <a class="dropdown-item" href="backend/user/rent.php">Alugar</a>
                <a class="dropdown-item" href="backend/user/cli_alugueres.php">Filmes Alugados</a>
                <a class="dropdown-item" style="color:red;" href="authenticate/logout.php">Logout</a>
              </div>
            </li>
          </ul>
        <?php
        } elseif ($cargoUtil == "Administrador") {
        ?>
          <ul style="margin-top: 0.5%; float: right;">
            <li class="nav-item dropdown ml-auto">
              <a class="nav-link dropdown-toggle" href="#" style="color: grey !important;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="backend/admin/perfil.php">Perfil</a>
              <a class="dropdown-item" href="backend/admin/rent.php">Alugar</a>
                <a class="dropdown-item" href="backend/admin/filmes.php">Filmes</a>
                <a class="dropdown-item" href="backend/admin/series.php">Séries</a>
                <a class="dropdown-item" href="backend/admin/atores.php">Atores</a>
                <a class="dropdown-item" href="backend/admin/AssociarFilmes.php">Ator-Filmes</a>
                <a class="dropdown-item" href="backend/admin/AssociarSeries.php">Ator-Series</a>
                <a class="dropdown-item" href="backend/admin/alugueres.php">Alugueres</a>
                <a class="dropdown-item" href="backend/admin/users.php">Utilizadores</a>
                <a class="dropdown-item" style="color:red;" href="authenticate/logout.php">Logout</a>
              </div>
            </li>
          </ul>
        <?php
        }
        ?>
      <?php
      } else {
      ?>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item">
            <button type="button" onclick="location.href='authenticate/login.php'" class="btn btn-outline-success">Login / Sign In</button>
          </li>
        </ul>
      <?php
      }
      ?>
    </div>
  </nav>


</head>


<body>

  <?php

  include("authenticate/config.php");





  $sql = "CREATE TABLE filmes (
    id SERIAL PRIMARY KEY,
    poster_url VARCHAR(255),
    banner_url VARCHAR(255),
    poster LONGBLOB,
    banner LONGBLOB,
    nome VARCHAR(255) NOT NULL UNIQUE KEY,
    rating VARCHAR(10),
    genero VARCHAR(255) NOT NULL,
    ano INT(4) NOT NULL,
    disponivel BOOLEAN DEFAULT TRUE,
    rent_by_user VARCHAR(255),
    synopse VARCHAR (255),
    trailer VARCHAR(255),
    writers VARCHAR(255) ,
    director VARCHAR(255) )";
  if ($conn->query($sql) === TRUE) echo "Tabela Filmes criada com sucesso!!!";
  else echo "Erro na criação da tabela: " . $conn->error;




  $sql = "CREATE TABLE series (
    id SERIAL PRIMARY KEY ,
    poster_url VARCHAR(255),
  banner_url VARCHAR(255),
  poster LONGBLOB,
  banner LONGBLOB,
    nome VARCHAR(255) NOT NULL UNIQUE KEY,
    rating VARCHAR(10),
    genero VARCHAR(255) NOT NULL,
    ano_inicio INT(4) NOT NULL,
    ano_fim INT(4) ,
    disponivel BOOLEAN DEFAULT TRUE,
    rent_by_user VARCHAR(255),
    synopse VARCHAR (255),
    trailer VARCHAR(255),
    writers VARCHAR(255) ,
    director VARCHAR(255) )";
  if ($conn->query($sql) === TRUE) echo "Tabela Series criada com sucesso!!!";
  else echo "Erro na criação da tabela: " . $conn->error;



  $sql = "CREATE TABLE atores (
      ator_id SERIAL PRIMARY KEY,
      foto LONGBLOB, 
      foto_url VARCHAR(255),
      nome VARCHAR(255))";
  if ($conn->query($sql) === TRUE) echo "Tabela Atores criada com sucesso!!!";
  else echo "Erro na criação da tabela: " . $conn->error;

  $sql = "CREATE TABLE atores_filmes (
    ator_filme_id SERIAL PRIMARY KEY,
    ator_id int(4) NOT NULL,
    film_id int(4) NOT NULL,
    personagem VARCHAR(255) )";
  if ($conn->query($sql) === TRUE) echo "Tabela atores_filmes criada com sucesso!!!";
  else echo "Erro na criação da tabela: " . $conn->error;

  $sql = "CREATE TABLE atores_series (
    ator_serie_id SERIAL PRIMARY KEY,
    ator_id int(4) NOT NULL,
    serie_id int(4) NOT NULL,
    personagem VARCHAR(255) )";
  if ($conn->query($sql) === TRUE) echo "Tabela atores_series criada com sucesso!!!";
  else echo "Erro na criação da tabela: " . $conn->error;



  $sql = "CREATE TABLE utilizadores (
  numero INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  login VARCHAR(30) NOT NULL UNIQUE KEY,
  password VARCHAR(100) NOT NULL,
  imagem LONGBLOB,
  token VARCHAR(200),
  nivel VARCHAR(255) DEFAULT('Utilizador') NOT NULL,
  email VARCHAR(30) NOT NULL UNIQUE KEY)";
  if ($conn->query($sql) === TRUE) echo "Tabela users criada com sucesso!!!";
  else echo "Erro na criação da tabela: " . $conn->error;

  $insert = $conn->query(" INSERT INTO  series(poster_url,nome,rating, genero, ano_inicio, ano_fim, trailer) VALUES ('https:ae01.alicdn.com/kf/HTB1ZSmqmsj_B1NjSZFHq6yDWpXab.jpg','Game of Thrones','9','Drama','2011','2019','https:www.youtube.com/embed/gcTkNV5Vg1E')");






// SERIES:

$insert = $conn->query("INSERT INTO  series(poster_url,nome,rating, genero, ano_inicio,trailer, director, synopse, writers) VALUES ('https:www.tvepipoca.com.br/wp-content/uploads/2019/01/miracle_workers-poster.jpg','Miracle Workers ','6.9','Comedy','2019','https:www.youtube.com/embed/bpWAyQQSm_k','Ryan Case, Maurice Marable, 
Jorma Taccone, 
Dan Schimpf', 'A comedy set in the offices of Heaven Inc. When God plans to destroy the Earth, two low-level angels must convince their boss to save humanity. They bet him they can pull off their most impossible miracle yet: help two humans fall in love.', 'Simon Rich');");







// FILMES

$insert = $conn->query("INSERT INTO  filmes(poster_url,banner_url,nome,rating, genero, ano,trailer ) VALUES ('https://vignette.wikia.nocookie.net/the-collectors/images/d/dc/Harry_Potter_and_the_Philosopher%27s_Stone_poster.jpg/revision/latest/scale-to-width-down/340?cb=20121031101002', 'https://www.flixwatch.co/wp-content/uploads/60004478.jpg','Harry Potter and the Philosophers Stone','7.7','Fantasy','2001','https://www.youtube.com/embed/VyHV0BRtdxo')");





$insert = $conn->query("INSERT INTO  filmes(poster_url,banner_url,nome,rating, genero, ano,trailer) VALUES ('https://images-na.ssl-images-amazon.com/images/I/514aVStnolL._AC_.jpg','http://assets.fanart.tv/fanart/movies/808/moviebackground/shrek-5316a50c754fb.jpg','Shrek','7.8','Animation','2001','https://www.youtube.com/embed/CwXOrWvPBPk')");





$insert = $conn->query("INSERT INTO  filmes(poster_url,banner_url,nome,rating, genero, ano,trailer) VALUES ('https://cdn.seat42f.com/wp-content/uploads/2018/05/01094131/IBIZA-Movie-Poster-Netflix.jpg','https://occ-0-1007-1009.1.nflxso.net/dnm/api/v6/E8vDc_W8CLv7-yMQu8KMEC7Rrr8/AAAABXkDIhPmdMwTrqaLzVa2JDwXMoEVbnFfB23KrPQkXqQzOWV1Wk_15UI9F1cplcYUSIBrSYhTQVv9JEq_Tb8PY6o19a4S.jpg?r=b1d','Ibiza','5.2','Comedy-Romance','2018','https://www.youtube.com/embed/Aw27eMJH3H4')");





$insert = $conn->query("INSERT INTO  filmes(poster_url,banner_url,nome,rating, genero, ano,trailer) VALUES ('https://m.media-amazon.com/images/M/MV5BMTMxMjY0NzE2M15BMl5BanBnXkFtZTcwNTc3ODcyMw@@._V1_SY1000_CR0,0,677,1000_AL_.jpg','https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/region_US/nbcu-10A17-Full-Image_GalleryCover-en-US-1516995448188._UR1920,1080_RI_.jpg','Shark Tale','6.0','Animation','2004','https://www.youtube.com/embed/mp2SbaK8dDg')");






// ATORES:

$insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMTg4NTExODc3Nl5BMl5BanBnXkFtZTgwODUyMDEzMDE@._V1_UY317_CR11,0,214,317_AL_.jpg','Daniel Radcliffe')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('1', '1', 'Harry Potter')");





 $insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMTgzNTA5ODg1NV5BMl5BanBnXkFtZTcwMDU3MTU5Mw@@._V1_.jpg','Richard Harris')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('2', '1', 'Albus Dumbledore')");





 $insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMjI3MDA3NjA1N15BMl5BanBnXkFtZTcwMDcyMDYzNw@@._V1_.jpg','Rupert Grint')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('3', '1', 'Ron Weasley'))");





 $insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMTY0MTM1MTM5Nl5BMl5BanBnXkFtZTcwNzA1OTM3MQ@@._V1_UX214_CR0,0,214,317_AL_.jpg','Mike Myers')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('4', '2', 'Shrek')");





 $insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMTc0NDQzODAwNF5BMl5BanBnXkFtZTYwMzUzNTk3._V1_UY317_CR6,0,214,317_AL_.jpg','Eddie Murphy')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('5', '2', 'Donkey')");





 $insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMTkxNTI5NzM4MV5BMl5BanBnXkFtZTcwMTI3ODY3Mg@@._V1_UY317_CR0,0,214,317_AL_.jpg','Cameron Diaz')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('6', '2', 'Princess Fiona')");





 $insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMTk5OTc5MTM1OV5BMl5BanBnXkFtZTcwMTU4NDI1NA@@._V1_UY317_CR8,0,214,317_AL_.jpg','Gillian Jacobs')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('7', '3', 'Harper')");





 $insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMWEzNjAzNzEtM2IwZC00OTEyLThkZTQtMDNlOTNjNTczY2Q2XkEyXkFqcGdeQXVyNTI5NjIyMw@@._V1_UY317_CR175,0,214,317_AL_.jpg','Phoebe Robinson')");





 $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('8', '3', '   Leah')");






$insert = $conn->query("INSERT INTO  atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BNjUyNDA0NDU4Ml5BMl5BanBnXkFtZTcwNzcyNzI4Ng@@._V1_UY317_CR131,0,214,317_AL_.jpg','Vanessa Bayer');");





$insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('9', '3', 'Nikki');");





$insert = $conn->query("INSERT INTO   atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BNTczMzk1MjU1MV5BMl5BanBnXkFtZTcwNDk2MzAyMg@@._V1_UY317_CR2,0,214,317_AL_.jpg','Will Smith');");





$insert = $conn->query("INSERT INTO  filmes(poster_url,banner_url,nome,rating, genero, ano,trailer) VALUES ('https://m.media-amazon.com/images/M/MV5BMTMxMjY0NzE2M15BMl5BanBnXkFtZTcwNTc3ODcyMw@@._V1_SY1000_CR0,0,677,1000_AL_.jpg','https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/region_US/nbcu-10A17-Full-Image_GalleryCover-en-US-1516995448188._UR1920,1080_RI_.jpg','Shark Tale','6.0','Animation','2004','https://www.youtube.com/embed/mp2SbaK8dDg')");





$insert = $conn->query("INSERT INTO   atores_filmes(ator_id, film_id, personagem) VALUES ('10', '4', 'Oscar ');");





$insert = $conn->query("INSERT INTO  atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMmI2NTU4NTYtNTdiZi00YjNhLTk5ZGUtMmFkZjBjYTliYjg1XkEyXkFqcGdeQXVyNzczNzg0ODc@._V1_UX214_CR0,0,214,317_AL_.jpg','Renée Zellweger');");





$insert = $conn->query("INSERT INTO   atores_filmes(ator_id, film_id, personagem) VALUES ('11', '4', ' Angie');");





$insert = $conn->query("INSERT INTO   atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMjAwNDU3MzcyOV5BMl5BanBnXkFtZTcwMjc0MTIxMw@@._V1_UY317_CR13,0,214,317_AL_.jpg','Robert De Niro');");






$insert = $conn->query("INSERT INTO  atores_filmes(ator_id, film_id, personagem) VALUES ('12', '4', 'Don Lino');");





$insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BYTFiMWNlNmItMDRiYy00NzA4LWE5YjItZmViNWQ2NzhhOWZlXkEyXkFqcGdeQXVyMTA2Mjc5ODMy._V1_UY317_CR16,0,214,317_AL_.jpg','Jack Black');");





$insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('13', '4', 'Lenny');");





$insert = $conn->query("INSERT INTO atores_series(ator_id, serie_id, personagem) VALUES ('1', '2','Craig');");





$insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMjE0ZWMyOTAtZmFmZi00NzllLWExNzctMWY2MjIzZjE4MDI2XkEyXkFqcGdeQXVyNTMxNjI3OTI@._V1_UX214_CR0,0,214,317_AL_.jpg','Geraldine Viswanathan');");





$insert = $conn->query("INSERT INTO  atores_series(ator_id, serie_id, personagem) VALUES ('14', '2', ' Eliza');");





$insert = $conn->query("INSERT INTO atores_series(ator_id, serie_id, personagem) VALUES ('15', '2', 'Sanjay');");





$insert = $conn->query("INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BMTg0NjQ1MTU1MF5BMl5BanBnXkFtZTgwNTAyMzc1MDE@._V1_UX214_CR0,0,214,317_AL_.jpg','Karan Soni');");





$insert = $conn->query("INSERT INTO  INSERT INTO atores (foto_url,nome) VALUES ('https:m.media-amazon.com/images/M/MV5BODc0NTU3NDA4M15BMl5BanBnXkFtZTcwNDkxNzQxNA@@._V1_UY317_CR11,0,214,317_AL_.jpg','Steve Buscemi');");





$insert = $conn->query("INSERT INTO atores_series(ator_id, serie_id, personagem) VALUES ('16', '2', 'God');");
  ?>




  <!------------------------------------------------------- CARROSSEL -------------------------------------------------------------------->


  <div class="container carousel-holder no-padding">
    <?php include("frontend/movies-search-form.php"); ?>
    <h1> Destaque </h1>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <?php

        $sql = "SELECT * FROM filmes ORDER BY ano desc limit 4";
        $result = $conn->query($sql);


        if ($result->num_rows > 0) {
          // output data of each row
          while ($row = $result->fetch_assoc()) {


            echo '<li data-target="#carouselExampleIndicators" data-slide-to="' . $row["id"] . '"></li>';
          }
        } else {
          echo "0 results";
        }
        ?>
        <!--
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li> -->
      </ol>
      <div class="carousel-inner" role="listbox">
        <?php

        $sql = "SELECT * FROM filmes order by ano desc limit 1";
        $result = $conn->query($sql);


        while ($row = $result->fetch_assoc()) {

          $id_first = $row["id"];

          if ($row["banner"] == NULL) {


            echo '
          <div class="carousel-item active ">
            <a href="frontend/sample.php?id=' . $row["id"] . '&type=filmes"><img class="d-block img-fluid" src="' . $row["banner_url"] . '" alt="' . $row["nome"] . '"></a>
          </div>';
          } else {
            echo '
          <div class="carousel-item active ">
            <a href="frontend/sample.php?id=' . $row["id"] . '&type=filmes"><img class="d-block img-fluid" src="data:image/jpeg;base64,' . base64_encode($row['banner']) . '" " alt="' . $row["nome"] . '"></a>
          </div>';
          }
        }
        ?>


        <?php

        $sql = "SELECT * FROM filmes where id not in (select id from filmes where id = $id_first) order by ano desc limit 4";
        $result = $conn->query($sql);


        if ($result->num_rows > 0) {
          // output data of each row
          while ($row = $result->fetch_assoc()) {

            if ($row["poster"] == NULL) {


              echo '
            <div class="carousel-item ">
              <a href="frontend/sample.php?id=' . $row["id"] . '&type=filmes"><img class="d-block img-fluid" src="' . $row["banner_url"] . '" alt="' . $row["nome"] . '"></a>
            </div>';
            } else {
              echo '
            <div class="carousel-item ">
              <a href="frontend/sample.php?id=' . $row["id"] . '&type=filmes"><img class="d-block img-fluid" src="data:image/jpeg;base64,' . base64_encode($row['banner']) . '" " alt="' . $row["nome"] . '"></a>
            </div>';
            }
          }
        } else {
          echo "0 results";
        }
        ?>

        <!-- 
        <div class="carousel-item">
          <img class="d-block img-fluid" src="assets/img/movies/carousel2.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="assets/img/movies/carousel1.jpg" alt="Third slide">
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="assets/img/movies/carousel2.jpg" alt="Forth slide">
        </div> -->
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>

  <!------------------------------------------------------- FILMES ----------------------------------------------------------->
  <div class="container">
    <h1> Últimos Inseridos </h1>
    <div class="box" style="border: 1px solid grey;border-radius: 5px;">
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#filmes">Filmes</a></li>
        <li><a data-toggle="tab" href="#series">Séries</a></li>
      </ul>
      <div class="overflow-box" style="overflow-y: auto;">

        <div class="tab-content">
          <div id="filmes" class="tab-pane fade in active">

            <div class="row">
              <?php

              $sql = "SELECT * FROM filmes order by id desc limit 12";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
                // output data of each row
                while ($row = $result->fetch_assoc()) {

                  if ($row["poster"] == NULL) {

                    echo '
                  <div class="col-4 col-md-2">
                    <a href="frontend/sample.php?id=' . $row["id"] . '&type=filmes"><img class="img-thumbnail" src="' . $row["poster_url"] . '" data-holder-rendered="true" style="width:100% height=100%; display: block;"></a>
                  </div>';
                  } else {

                    echo '
                  <div class="col-4 col-md-2">
                    <a href="frontend/sample.php?id=' . $row["id"] . '&type=filmes"><img class="img-thumbnail" src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" data-holder-rendered="true" style="width:100% height=100%; display: block;"></a>
                  </div>';
                  }
                }
              } else {
                echo 'No movies';
              }
              ?>

            </div>
          </div>
          <div id="series" class="tab-pane fade">

            <div class="row">

              <?php

              $sql = "SELECT * FROM series order by id desc limit 12";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
                // output data of each row
                while ($row = $result->fetch_assoc()) {

                  if ($row["poster"] == NULL) {

                    echo '
                  <div class="col-4 col-md-2">
                    <a href="frontend/sample.php?id=' . $row["id"] . '&type=series"><img class="img-thumbnail" src="' . $row["poster_url"] . '" data-holder-rendered="true" style="width:100% height=100%; display: block;"></a>
                  </div>';
                  } else {

                    echo '
                  <div class="col-4 col-md-2">
                    <a href="frontend/sample.php?id=' . $row["id"] . '&type=series"><img class="img-thumbnail" src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" data-holder-rendered="true" style="width:100% height=100%; display: block;"></a>
                  </div>';
                  }
                }
              } else {
                echo 'No series';
              }
              ?>

            </div>
          </div>
        </div>
      </div>

    </div>

  </div>



</body>

<footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</footer>

</html>