<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


  <?php
  include("../../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

  if ($autenticado) {
  ?>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="../../index.php">
        PeanutFlix
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="../../frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Filmes
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="../../frontend/movies.php">All</a>
              <a class="dropdown-item" href="../../frontend/movies_generos.php">Generos</a>
              <a class="dropdown-item" href="#"></a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Séries
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="../../frontend/tv.php">All</a>
              <a class="dropdown-item" href="../../frontend/tv_generos.php">Generos</a>
              <a class="dropdown-item" href="#"></a>
            </div>
          </li>
        </ul>
        <?php

        ?>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item dropdown ml-auto">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="perfil.php">Perfil</a>
              <a class="dropdown-item" href="rent.php">Alugar</a>
              <a class="dropdown-item" href="cli_alugueres.php">Filmes Alugados</a>
              <a class="dropdown-item" style="color:red;" href="../../authenticate/logout.php">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
</head>


<body>

  <?php

    include("../../authenticate/config.php");

    if (isset($_GET['novoId'])) {
      $id_filme = $_GET['novoId'];
      $type = $_GET['type'];

      if ($type == "filmes") {
        $sql = "UPDATE filmes SET disponivel=0, rent_by_user='$idUtil' WHERE id=$id_filme";

        if ($conn->query($sql) === TRUE) {
          echo "Record deleted successfully";
          header("Location: rent.php");
          die();
        } else {
          echo "Error deleting record: " . $conn->error;
        }

        $conn->close();
      } else if ($type == "series") {

        $sql = "UPDATE series SET disponivel=0, rent_by_user='$idUtil' WHERE id=$id_filme";

        if ($conn->query($sql) === TRUE) {
          echo "Record deleted successfully";
          header("Location: rent.php");
          die();
        } else {
          echo "Error deleting record: " . $conn->error;
        }

        $conn->close();
      }
    }

  ?>


  <div class="row">
    <div class="col-md-6">
      <h2>Todos os filmes:</h2>
      <form action="rent.php" method="post" style="margin-bottom: 2%">
        <input type="text" name="search">
        <input type="submit" value="Search">
      </form>
      <div class="tableFixHead" style="overflow-y: auto; height: 500px">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Poster</th>
              <th scope="col">Nome</th>
              <th scope="col">Género</th>
              <th scope="col">Disponibilidade</th>
              <th scope="col">Ano</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $search = $_POST["search"];


            $sql = "SELECT * FROM filmes where nome LIKE '%$search%' and disponivel=1 ORDER BY ano desc";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
              // output data of each row
              while ($row = $result->fetch_assoc()) {
                echo '<tr>
              <td scope="row">' . $row["id"] . '</td>';

                $id = $row["id"];

                $sql2 = "SELECT * FROM filmes WHERE id='$id' ORDER BY id DESC";
                $sqlData = mysqli_query($conn, $sql2);
                $result2 = mysqli_fetch_array($sqlData);


                if ($row["poster"] == NULL) {
                  echo '<td><img src="' . $row["poster_url"] . '" style="height: 90px" /></td>';
                } else {
                  echo '<td><img src="data:image/jpeg;base64,' . base64_encode($result2['poster']) . '" style="height: 90px" /></td>';
                }
                //echo '<td><img style="height: 90px;" src="data:image/jpeg;base64,' . base64_encode($result2['poster']) . '"/></td>';
                //<td> <img src=" ' . $row["poster"] . ' " style="height: 90px" / </td>
                echo '<td> ' . $row["nome"] . '</td>
              <td> ' . $row["genero"] . '</td>';
                if ($row["disponivel"] == 1) {
                  echo '<td> Disponível </td>';
                } else {
                  echo '<td> Indisponível </td>';
                }
                echo '<td> ' . $row["ano"] . '</td>';
                if ($row["disponivel"] == 1) {
                  echo '<td> ' .
                    '<a href="rent.php?novoId=' . $row["id"] . '&type=filmes"><button class="btn btn-primary">Rent</button></a> ' .
                    ' </td>
            </tr>';
                }
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>


    <div class="col-md-6">
      <h2>Todas as séries:</h2>
      <form action="rent.php" method="post" style="margin-bottom: 2%">
        <input type="text" name="Ssearch">
        <input type="submit" value="Search">
      </form>
      <div class="tableFixHead" style="overflow-y: auto; height: 500px">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Poster</th>
              <th scope="col">Nome</th>
              <th scope="col">Género</th>
              <th scope="col">Disponibilidade</th>
              <th scope="col">Ano Inícial</th>
              <th scope="col">Ano Final</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $Ssearch = $_POST["Ssearch"];

            $sql = "SELECT * FROM series where nome LIKE '%$Ssearch%' and disponivel=1 ORDER BY ano_fim DESC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
              // output data of each row
              while ($row = $result->fetch_assoc()) {
                echo '<tr>
              <td scope="row">' . $row["id"] . '</td>';

                $id = $row["id"];

                $sql2 = "SELECT * FROM series WHERE id='$id' ORDER BY id DESC";
                $sqlData = mysqli_query($conn, $sql2);
                $result2 = mysqli_fetch_array($sqlData);

                if ($row["poster"] == NULL) {
                  echo '<td><img src="' . $row["poster_url"] . '" style="height: 90px" /></td>';
                } else {
                  echo '<td><img src="data:image/jpeg;base64,' . base64_encode($result2['poster']) . '" style="height: 90px" /></td>';
                }
                //<td> <img src=" ' . $row["poster"] . ' " style="height: 90px" / </td>
                echo '<td> ' . $row["nome"] . '</td>
              <td> ' . $row["genero"] . '</td>';
                if ($row["disponivel"] == 1) {
                  echo '<td> Disponível </td>';
                } else {
                  echo '<td> Indisponível </td>';
                }
                echo '<td> ' . $row["ano_inicio"] . '</td>
              <td> ' . $row["ano_fim"] . '</td>';
                if ($row["disponivel"] == 1) {
                  echo '<td> ' .
                    ' <a href="rent.php?novoId=' . $row["id"] . '&type=series"><button class="btn btn-primary">Rent</button></a> ' .
                    ' </td>
            </tr>';
                }
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>

  </div>

<?php
  } else {
?>
  <p>Página Indisponivel</p>
<?php
  }
?>


</body>


<footer>
  <?php
  $conn->close();
  ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

</footer>

</html>