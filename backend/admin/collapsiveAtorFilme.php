<style>
    /* Style the button that is used to open and close the collapsible content */
    .collapsible {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
    }

    /* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
    .active,
    .collapsible:hover {
        background-color: #ccc;
    }

    .collapsible:after {
        content: '\02795';
        /* Unicode character for "plus" sign (+) */
        font-size: 13px;
        color: white;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2796";
        /* Unicode character for "minus" sign (-) */
    }

    /* Style the collapsible content. Note: hidden by default */
    .content {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
</style>

<?php

$erro_idAtor = "";
$erro_idFilm = "";
$erro_personagem = "";

if (isset($_POST['add'])) {

    include("../../authenticate/config.php");

    $id_filme = filter_var($_POST['film_id'], FILTER_SANITIZE_STRING);
    $id_ator = filter_var($_POST['ator_id'], FILTER_SANITIZE_STRING);
    $personagem = filter_var($_POST['personagem'], FILTER_SANITIZE_STRING);


    //------------------------LOGIN-------------------------------------------------------------------------------------

    if (empty($id_filme)) {
        $erro_idFilm = "Insira dados no campo nome";
    }

    if (empty($id_ator)) {
        $erro_idAtor = "Insira dados no campo nome";
    }

    if (empty($personagem)) {
        $erro_personagem = "Insira dados no campo nome";
    }


    //------------------------INSERÇAO----------------------------------------------------------------------------------

    if (($erro_idAtor == "")&& ($erro_idFilm == "")&& ($erro_personagem == "")) {

        $insert = $conn->query("INSERT INTO atores_filmes(ator_id, film_id, personagem) VALUES ('$id_ator', '$id_filme','$personagem');");
        if ($insert) {
            $conn->close();
            header("Location: http://saw.com/peanutflix/backend/admin/atores.php");
            die();
        } else {
            echo "File upload failed, please try again";
        }
    } else {
        echo "Oops! Something went wrong";
    }
}
?>

<button type="button" class="collapsible">Associar Ator-Filme</button>
<div class="content">
    <hr>
    <form role="form" method="post" enctype="multipart/form-data">
        
        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Id Filme: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="film_id" required />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Id Ator: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="ator_id" required />
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Personagem: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="personagem" required />
            </div>
        </div>

        <hr>
        <button class="btn btn-success" style="margin-bottom:20px; float:right;" name="add">Associar</button>
    </form>
</div>
<br>

<script>
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
</script>