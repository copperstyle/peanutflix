<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


  <?php
  include("../../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

  if (($autenticado) && ($cargoUtil == "Administrador")) {
  ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="../../index.php">
        PeanutFlix
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="perfil.php">Perfil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rent.php">Alugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="filmes.php">Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="series.php">Séries</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="atores.php">Atores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarFilmes.php">Ator-Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarSeries.php">Ator-Series</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="alugueres.php">Alugueres</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">Utilizadores</a>
          </li>
        </ul>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item dropdown ml-auto">
            <a style="color:red;" href="../../authenticate/logout.php">Logout</a>
          </li>
        </ul>

      </div>
    </nav>
</head>


<body>
  <?php

    include("../../authenticate/config.php");

    $id = "";
    if (isset($_GET["id"])) {
      $id = $_GET["id"];
    }

    $erro_imagem = "";
    $erro_nome = "";
    $erro_genero = "";
    $erro_ano = "";
    $erro_sinopse = "";
    $erro_trailer = "";
    $erro_writers = "";
    $erro_director = "";
    $erro_rating = "";
    $erro_posterURL = "";

    if (isset($_POST['update'])) {


      $nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
      $genero = filter_var($_POST['genero'], FILTER_SANITIZE_STRING);
      $ano =  filter_var($_POST['ano'], FILTER_SANITIZE_STRING);
      $sinopse =  filter_var($_POST['sinopse']);
      $trailer =  filter_var($_POST['trailer'], FILTER_SANITIZE_STRING);
      $writers =  filter_var($_POST['writers'], FILTER_SANITIZE_STRING);
      $director =  filter_var($_POST['director'], FILTER_SANITIZE_STRING);
      $rating = filter_var($_POST['rating'], FILTER_SANITIZE_STRING);

      //------------------------Nome-------------------------------------------------------------------------------------

      if (empty($nome)) {
        $erro_nome = "Insira dados no campo nome";
      }

      //------------------------Genero----------------------------------------------------------------------------------

      if (empty($genero)) { //check if string is empty
        $erro_genero = "Insira dados no campo genero";
      }

      //------------------------Ano-------------------------------------------------------------------------------------

      if (empty($ano)) { //check if string is empty
        $erro_ano = "Insira dados no campo de ano";
      }

      //------------------------Synopse-------------------------------------------------------------------------------------

      if (empty($sinopse)) { //check if string is empty
        $erro_sinopse = "Insira dados no campo de sinopse";
      }

      //------------------------Trailer-------------------------------------------------------------------------------------

      if (empty($trailer)) { //check if string is empty
        $erro_trailer = "Insira dados no campo de trailer";
      }

      //------------------------Writers-------------------------------------------------------------------------------------

      if (empty($writers)) { //check if string is empty
        $erro_writers = "Insira dados no campo de writers";
      }

      //------------------------Director-------------------------------------------------------------------------------------

      if (empty($director)) { //check if string is empty
        $erro_director = "Insira dados no campo de diretor";
      }

      //------------------------INSERÇAO----------------------------------------------------------------------------------

      if (($erro_nome == "") && ($erro_genero == "") && ($erro_ano == "") && ($erro_sinopse == "") && ($erro_trailer == "") && ($erro_writers == "") && ($erro_director == "") && ($erro_rating == "")) {
        

        $sql = "UPDATE filmes SET nome='$nome', genero='$genero', ano='$ano', synopse='$sinopse', trailer='$trailer', writers='$writers', director='$director', rating='$rating' WHERE id='$id'";
        if ($conn->query($sql) === TRUE) {
          $sucesso = "Dados atualizados com sucesso";
          header("Location: filmes.php");
          die();
        } else {
          echo "Error updating record: " . $conn->error;
        }
      } else {
        echo "Oops! Something went wrong";
      }
    }


    $sql = "SELECT * FROM filmes WHERE id = $id";
    $sqlData = mysqli_query($conn, $sql);
    $result = mysqli_fetch_array($sqlData);
  ?>

  <div class="container">

    <form role="form" method="post" enctype="multipart/form-data">
      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Nome: </label>
        </div>
        <div class="col-md-10">
          <input type="text" value="<?php echo $result["nome"] ?>" style="width:100%" name="nome" required />
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Género: </label>
        </div>
        <div class="col-md-10">
          <input type="text" value="<?php echo $result["genero"] ?>" style="width:100%" name="genero" required />
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Ano: </label>
        </div>
        <div class="col-md-10">
          <input type="text" value="<?php echo $result["ano"] ?>" style="width:100%" name="ano" required />
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Rating: </label>
        </div>
        <div class="col-md-10">
          <input type="text" value="<?php echo $result["rating"] ?>" style="width:100%" name="rating" required />
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Sinopse: </label>
        </div>
        <div class="col-md-10">
          <textarea rows="10" style="width:100%" name="sinopse" scols="50" required><?php echo $result["synopse"] ?></textarea>
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Trailer: </label>
        </div>
        <div class="col-md-10">
          <input type="text" value="<?php echo $result["trailer"] ?>" style="width:100%" name="trailer" required />
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Writers: </label>
        </div>
        <div class="col-md-10">
          <input type="text" value="<?php echo $result["writers"] ?>" style="width:100%" name="writers" required />
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-md-2">
          <label>Director: </label>
        </div>
        <div class="col-md-10">
          <input type="text" value="<?php echo $result["director"] ?>" style="width:100%" name="director" required />
        </div>
      </div>

      <hr>
      <button class="btn btn-primary" style="margin-bottom:20px; float:right;" name="update">Atualizar</button>
    </form>
  </div>

<?php
  } else {
?>
  <p>Página Indisponivel</p>
<?php
  }
?>


</body>


<footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

</footer>

</html>