<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


  <?php
  include("../../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

  if (($autenticado) && ($cargoUtil == "Administrador")) {
  ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="../../index.php">
        PeanutFlix
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="perfil.php">Perfil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rent.php">Alugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="filmes.php">Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="series.php">Séries</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="atores.php">Atores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarFilmes.php">Ator-Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarSeries.php">Ator-Series</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="alugueres.php">Alugueres</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">Utilizadores</a>
          </li>
        </ul>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item dropdown ml-auto">
            <a style="color:red;" href="../../authenticate/logout.php">Logout</a>
          </li>
        </ul>

      </div>
    </nav>
</head>


<body>



  <?php

    include("../../authenticate/config.php");


    $user = "";
    $username = "";
    if (isset($_GET['user'])) {
      $user = $_GET['user'];

      $sql = "SELECT * FROM utilizadores WHERE numero='$user'";
      $result = $conn->query($sql);
      $row = $result->fetch_assoc();
      $username = $row["login"];
    }

  ?>


  <div class="row">



    <div class="col-md-6">
      <h2>Filmes Alugados pelo cliente <?php echo $username ?>:</h2>

      <div class="tableFixHead" style="overflow-y: auto; height: 500px">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Poster</th>
              <th scope="col">Nome</th>
              <th scope="col">Género</th>
              <th scope="col">Disponibilidade</th>
              <th scope="col">Cliente</th>
              <th scope="col">Ano</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $sql = "SELECT * FROM filmes WHERE disponivel=0 AND rent_by_user='$user' ORDER BY id DESC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
              // output data of each row
              while ($row = $result->fetch_assoc()) {
                echo '<tr>
              <td scope="row">' . $row["id"] . '</td>';

                $id = $row["id"];
                $disponibilidade = $row["disponivel"];


                $sql2 = "SELECT * FROM filmes WHERE id='$id' AND disponivel=0 AND rent_by_user='$user' ORDER BY id DESC";
                $sqlData = mysqli_query($conn, $sql2);
                $result2 = mysqli_fetch_array($sqlData);

                if ($row["poster"] == NULL) {
                  echo '<td><img src="' . $row["poster_url"] . '" style="height: 90px" /></td>';
                } else {
                  echo '<td><img src="data:image/jpeg;base64,' . base64_encode($result2['poster']) . '" style="height: 90px" /></td>';
                }
                //<td> <img src=" ' . $row["poster"] . ' " style="height: 90px" / </td>
                echo '<td> ' . $row["nome"] . '</td>
              <td> ' . $row["genero"] . '</td>';
                if ($row["disponivel"] == 1) {
                  echo '<td> Disponível </td>';
                } else {
                  echo '<td> Indisponível </td>';
                }
                echo '<td> ' . $username . '</td>
              <td> ' . $row["ano"] . '</td>
            </tr>';
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-6">
      <h2>Series Alugados pelo cliente <?php echo $username ?>:</h2>
      <div class="tableFixHead" style="overflow-y: auto; height: 500px">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Poster</th>
              <th scope="col">Nome</th>
              <th scope="col">Género</th>
              <th scope="col">Disponibilidade</th>
              <th scope="col">Ano</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $sql = "SELECT * FROM series WHERE disponivel=0 AND rent_by_user='$user' ORDER BY id DESC";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
              // output data of each row
              while ($row = $result->fetch_assoc()) {
                echo '<tr>
              <td scope="row">' . $row["id"] . '</td>';

                $id = $row["id"];
                $disponibilidade = $row["disponivel"];


                $sql2 = "SELECT * FROM series WHERE id='$id' AND disponivel=0 AND rent_by_user='$user' ORDER BY id DESC";
                $sqlData = mysqli_query($conn, $sql2);
                $result2 = mysqli_fetch_array($sqlData);


                if ($row["poster"] == NULL) {
                  echo '<td><img src="' . $row["poster_url"] . '" style="height: 90px" /></td>';
                } else {
                  echo '<td><img src="data:image/jpeg;base64,' . base64_encode($result2['poster']) . '" style="height: 90px" /></td>';
                }
                //echo '<td><img style="height: 90px;" src="data:image/jpeg;base64,' . base64_encode($result2['poster']) . '"/></td>';
                //<td> <img src=" ' . $row["poster"] . ' " style="height: 90px" / </td>
                echo '<td> ' . $row["nome"] . '</td>
              <td> ' . $row["genero"] . '</td>';
                if ($row["disponivel"] == 1) {
                  echo '<td> Disponível </td>';
                } else {
                  echo '<td> Indisponível </td>';
                }
                echo '
              <td> ' . $row["ano_inicio"] . ' - ' . $row["ano_inicio"] . '</td>
            </tr>';
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>

  </div>
<?php
  } else {
?>
  <p>Página Indisponivel</p>
<?php
  }
?>


</body>


<footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

</footer>

</html>