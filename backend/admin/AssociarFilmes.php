<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


  <?php
  include("../../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

  if (($autenticado) && ($cargoUtil == "Administrador")) {
  ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="../../index.php">
        PeanutFlix
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="perfil.php">Perfil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rent.php">Alugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="filmes.php">Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="series.php">Séries</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="atores.php">Atores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarFilmes.php">Ator-Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarSeries.php">Ator-Series</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="alugueres.php">Alugueres</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">Utilizadores</a>
          </li>
        </ul>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item dropdown ml-auto">
            <a style="color:red;" href="../../authenticate/logout.php">Logout</a>
          </li>
        </ul>

      </div>
    </nav>
</head>


<body>


  <div class="container">


    <?php

    include("../../authenticate/config.php");


    if (isset($_GET['novoId'])) {
      $id_ator = $_GET['novoId'];

      $sql = "DELETE FROM atores WHERE ator_id=$id_ator";

      if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
        header("Location: http://saw.com/peanutflix/backend/admin/atores.php");
        die();
      } else {
        echo "Error deleting record: " . $conn->error;
      }

      $conn->close();
    }

    //Botão de collapse
    include("collapsiveAtorFilme.php");
    ?>


    <div class="row">
      <div class="col-md-6">
        <h2>Todos os Atores:</h2>
        <form action="AssociarFilmes.php" method="post" style="margin-bottom: 2%">
          <input type="text" name="Asearch">
          <input type="submit" value="Search">
        </form>

        <div class="tableFixHead" style="overflow-y: auto; height: 380px">
          <table class="table table-hover">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Foto</th>
                <th scope="col">Nome</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $Asearch = $_POST["Asearch"];

              $sql = "SELECT * FROM atores where nome LIKE '%$Asearch%' order by ator_id ASC";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
                // output data of each row
                while ($row = $result->fetch_assoc()) {
                  echo '<tr>
              <td scope="row">' . $row["ator_id"] . '</td>';

                  $id = $row["ator_id"];

                  $sql2 = "SELECT * FROM atores WHERE ator_id='$id' ORDER BY ator_id asc";
                  $sqlData = mysqli_query($conn, $sql2);
                  $result2 = mysqli_fetch_array($sqlData);

                  if ($row["foto"] == NULL) {
                    echo '<td> <img src=" ' . $row["foto_url"] . ' " style="height: 90px" / </td>';
                  } else {

                    echo '<td><img style="height: 90px;" src="data:image/jpeg;base64,' . base64_encode($result2['foto']) . '"/></td>';
                  }
                  echo '<td> ' . $row["nome"] . '</td>
             ';
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-6">
        <h2>Todos os Filmes:</h2>
        <form action="AssociarFilmes.php" method="post" style="margin-bottom: 2%">
          <input type="text" name="search">
          <input type="submit" value="Search">
        </form>
        <div class="tableFixHead" style="overflow-y: auto; height: 380px">

          <table class="table table-hover">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Poster</th>
                <th scope="col">Nome</th>
                <th scope="col">Género</th>
                <th scope="col">Ano</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $search = $_POST["search"];

              $sql = "SELECT * FROM filmes where nome LIKE '%$search%' order by id ASC";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
                // output data of each row
                while ($row = $result->fetch_assoc()) {
                  echo '<tr>
              <td scope="row">' . $row["id"] . '</td>';

                  $id = $row["id"];

                  $sql2 = "SELECT * FROM filmes WHERE id='$id' ORDER BY id ASC";
                  $sqlData = mysqli_query($conn, $sql2);
                  $result2 = mysqli_fetch_array($sqlData);

                  if ($row["poster"] == NULL) {
                    echo '<td> <img src=" ' . $row["poster_url"] . ' " style="height: 90px" / </td>';
                  } else {

                    echo '<td><img style="height: 90px;" src="data:image/jpeg;base64,' . base64_encode($result2['poster']) . '"/></td>';
                  }


                  //
                  echo '<td> ' . $row["nome"] . '</td>
              <td> ' . $row["genero"] . '</td>';

                  echo '<td> ' . $row["ano"] . '</td>
              ';
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>

<?php
  } else {
?>
  <p>Página Indisponivel</p>
<?php
  }
?>


</body>


<footer>
  <?php
  $conn->close();
  ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

</footer>

</html>