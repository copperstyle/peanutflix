<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


  <?php
  include("../../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

  $msg_imagem = "";
  $erro_email = "";
  $erro_username = "";
  $erro_password = "";
  if (isset($_POST['save'])) {

    include("../../authenticate/config.php");

    //TODO if caso os campos estejam vazios n fzr nada, verificar se n existe um username assim, verificar se a password esta correta
    $novo_email = $_POST['update_email'];
    $novo_username = $_POST['update_username'];
    $update_password = $_POST['password'];
    $update_repassword = $_POST['re_password'];

    if (empty($novo_email)) {
      $erro_email = "O campo não pode estar vazio";
    }

    $sql_search1 = "SELECT email FROM utilizadores WHERE email='$novo_email'";
    $result_search1 = $conn->query($sql_search1);
    if (($novo_email != $emailUtil) && ($result_search1->num_rows != 0)) {
      $erro_email = "Email já existente";
    }

    $sql_search2 = "SELECT login FROM utilizadores WHERE login='$novo_username'";
    $result_search2 = $conn->query($sql_search2);
    if (($novo_username != $nomeUtil) && ($result_search2->num_rows != 0)) {
      $erro_username = "Username já existente";
    }

    if ($update_password != $update_repassword) {
      $erro_password = "As passwords não coincidem";
    } else if (!password_verify($update_password, $passwordUtil)) {
      $erro_password = "A password inserida não é válida";
    }

    if (($erro_email == "") && ($erro_username == "") && ($erro_password == "")) {
      $sql = "UPDATE utilizadores SET email='$novo_email', login='$novo_username' WHERE numero='$idUtil'";
      if ($conn->query($sql) === TRUE) {
        $sucesso = "Dados atualizados com sucesso";
        header("Location: perfil.php");
        die();
      } else {
        echo "Error updating record: " . $conn->error;
      }
    }

    $conn->close();
  }

  if (($autenticado) && ($cargoUtil == "Administrador")) {
  ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="../../index.php">
        PeanutFlix
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
            <a class="nav-link" href="perfil.php">Perfil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rent.php">Alugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="filmes.php">Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="series.php">Séries</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="atores.php">Atores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarFilmes.php">Ator-Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarSeries.php">Ator-Series</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="alugueres.php">Alugueres</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">Utilizadores</a>
          </li>
        </ul>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item dropdown ml-auto">
            <a style="color:red;" href="../../authenticate/logout.php">Logout</a>
          </li>
        </ul>





      </div>
    </nav>
</head>


<body>

  <?php
    if (isset($_POST["upload"])) {
      $check = getimagesize($_FILES["image"]["tmp_name"]);
      if ($check !== false) {

        $image = $_FILES["image"]["tmp_name"];
        $imgContent = addslashes(file_get_contents($image));

        include("../../authenticate/config.php");

        $insert = $conn->query("UPDATE utilizadores SET imagem='$imgContent' WHERE numero='$idUtil'");
        if ($insert) {
          $msg_imagem = "File uploaded successfully.";
        } else {
          $msg_imagem = "File upload failed, please try again";
        }
      } else {
        echo "Please select an image file to upload";
      }
    }
  ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4 col-md-5">
        <div class="text-center card-box">
          <div class="member-card">
            <div class="thumb-xl member-thumb m-b-10 center-block">
              <?php
              include("../../authenticate/config.php");

              $sql = "SELECT * FROM utilizadores WHERE numero = $idUtil";
              $sqlData = mysqli_query($conn, $sql);
              $result = mysqli_fetch_array($sqlData);

              echo '<img style="width:200px;height:200px;" src="data:image/jpeg;base64,' . base64_encode($result['imagem']) . '"/>';
              ?>

            </div>


            <div class="">
              <br>
              <form method="post" enctype="multipart/form-data">
                <div class="btn-toolbar mb-3 mb-md-0">
                  Select image to upload:
                  <input type="file" name="image" />
                  <input type="submit" name="upload" value="UPLOAD" />
                  <br>
                  <span class="error"> <?php echo $msg_imagem; ?> </span>
              </form>
            </div>
          </div>

          <!-- <div class="text-left m-t-40">
              <p class="text-muted font-13"><strong>Email:</strong> <span class="m-l-15"><?php echo $emailUtil ?></span>
              </p>
              <p class="text-muted font-13"><strong>Location:</strong> <span class="m-l-15">Portugal</span></p>
            </div> -->
        </div>
      </div> <!-- end card-box -->
    </div> <!-- end col -->


    <div class="col-md-7 col-lg-8">
      <div class="">
        <div class="">
          <div class="tab-content">
            <form role="form" method="post">
              <div class="form-group">
                <label for="Email">Email</label>
                <input type="email" name="update_email" value="<?php echo $emailUtil ?>" id="Email" class="form-control" required>
                <span class="error"> <?php echo $erro_email; ?> </span>
              </div>
              <div class="form-group">
                <label for="Username">Username</label>
                <input type="text" name="update_username" value="<?php echo $nomeUtil ?>" id="Username" class="form-control" required>
                <span class="error"> <?php echo $erro_username; ?> </span>
              </div>
              <div class="form-group">
                <label for="Password">Password</label>
                <input type="password" name="password" placeholder="6 - 15 Characters" id="Password" class="form-control" required>
                <span class="error"> <?php echo $erro_password; ?> </span>
              </div>
              <div class="form-group">
                <label for="RePassword">Re-Password</label>
                <input type="password" name="re_password" placeholder="6 - 15 Characters" id="RePassword" class="form-control" required>
              </div>
              <button class="btn btn-primary waves-effect waves-light w-md" type="submit" name="save">Save</button>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div> <!-- end col -->
  </div>
<?php
  } else {
?>
  <p>Página Indisponivel</p>
<?php
  }
?>


</body>


<footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

</footer>

</html>