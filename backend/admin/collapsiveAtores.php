<style>
    /* Style the button that is used to open and close the collapsible content */
    .collapsible {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
    }

    /* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
    .active,
    .collapsible:hover {
        background-color: #ccc;
    }

    .collapsible:after {
        content: '\02795';
        /* Unicode character for "plus" sign (+) */
        font-size: 13px;
        color: white;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2796";
        /* Unicode character for "minus" sign (-) */
    }

    /* Style the collapsible content. Note: hidden by default */
    .content {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
</style>

<?php

$erro_imagem = "";
$erro_nome = "";
$erro_fotoURL = "";

if (isset($_POST['add'])) {

    include("../../authenticate/config.php");

    $nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
    $foto_url = filter_var($_POST['foto_url']);


    //------------------------IMAGEM-------------------------------------------------------------------------------------

    $check = getimagesize($_FILES["foto"]["tmp_name"]);
    if ($check !== false) {
        $image = $_FILES["foto"]["tmp_name"];
        $imgContent = addslashes(file_get_contents($image));
    } else {
        $erro_imagem = "Porfavor selecione uma imagem para inserir";
    }

    //------------------------LOGIN-------------------------------------------------------------------------------------

    if (empty($foto_url)) {
        $erro_fotoURL = "Insira dados no campo nome";
    }

    if (empty($nome)) {
        $erro_nome = "Insira dados no campo nome";
    }


    //------------------------INSERÇAO----------------------------------------------------------------------------------

    if (($erro_nome == "")&& ($erro_fotoURL == "")) {

        $insert = $conn->query("INSERT INTO atores(foto, nome, foto_url) VALUES ('$imgContent', '$nome','$foto_url')");
        if ($insert) {
            $conn->close();
            header("Location: atores.php");
            die();
        } else {
            echo "File upload failed, please try again";
        }
    } else {
        echo "Oops! Something went wrong";
    }
}
?>

<button type="button" class="collapsible">Add Ator</button>
<div class="content">
    <hr>
    <form role="form" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-2">
                <label>Imagem do Ator: </label>
            </div>
            <div class="col-md-10">
                <input type="file" name="foto" />
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Imagem do Ator URL: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="foto_url" required />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Nome: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="nome" required />
            </div>
        </div>

        <hr>
        <button class="btn btn-success" style="margin-bottom:20px; float:right;" name="add">Adicionar</button>
    </form>
</div>
<br>

<script>
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
</script>