<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


  <?php
  include("../../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

  if (($autenticado) && ($cargoUtil == "Administrador")) {
  ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="../../index.php">
        PeanutFlix
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="perfil.php">Perfil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rent.php">Alugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="filmes.php">Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="series.php">Séries</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="atores.php">Atores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarFilmes.php">Ator-Filmes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="AssociarSeries.php">Ator-Series</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="alugueres.php">Alugueres</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">Utilizadores</a>
          </li>
        </ul>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item dropdown ml-auto">
            <a style="color:red;" href="../../authenticate/logout.php">Logout</a>
          </li>
        </ul>

      </div>
    </nav>
</head>


<body>


  <div class="container">

    <div class="row">
      <?php

      include("../../authenticate/config.php");


      if (isset($_GET['novoNum'])) {
        $id_user = $_GET['novoNum'];

        $sql = "DELETE FROM utilizadores WHERE numero=$id_user";

        if ($conn->query($sql) === TRUE) {
          echo "Record deleted successfully";
          header("Location: users.php");
          die();
        } else {
          echo "Error deleting record: " . $conn->error;
        }

        $conn->close();
      }
      ?>
      <form action="users.php" method="post" style="margin-bottom: 2%">
          <input type="text" name="search">
          <input type="submit" value="Search">
        </form>

      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">Cargo</th>
            <th scope="col">Email</th>
            <th scope="col">Opções</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $search = $_POST["search"];

          $sql = "SELECT * FROM utilizadores where login LIKE '%$search%' ";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
              echo '<tr>
              <td scope="row">' . $row["numero"] . '</td>
              <td> ' . $row["login"] . ' </td>
              <td> ' . $row["nivel"] . '</td>
              <td> ' . $row["email"] . '</td>
              <td> ' .
                ' <a href="cli_alugueres.php?user=' . $row["numero"] . '"><button type="button" class="btn btn-info"> Alugados </button></a> ' .
                ' <a href="users.php?novoNum=' . $row["numero"] . '"><button class="btn btn-danger">Eliminar</button></a>' .
                ' </td>
            </tr>';
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
<?php
  } else {
?>
  <p>Página Indisponivel</p>
<?php
  }
?>


</body>


<footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

</footer>

</html>