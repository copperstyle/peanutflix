<style>
    /* Style the button that is used to open and close the collapsible content */
    .collapsible {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
    }

    /* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
    .active,
    .collapsible:hover {
        background-color: #ccc;
    }

    .collapsible:after {
        content: '\02795';
        /* Unicode character for "plus" sign (+) */
        font-size: 13px;
        color: white;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2796";
        /* Unicode character for "minus" sign (-) */
    }

    /* Style the collapsible content. Note: hidden by default */
    .content {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
</style>

<?php

$erro_imagem = "";
$erro_nome = "";
$erro_genero = "";
$erro_ano_inicio = "";
$erro_ano_fim = "";
$erro_sinopse = "";
$erro_trailer = "";
$erro_writers = "";
$erro_director = "";
$erro_rating = "";
$erro_posterURL = "";

if (isset($_POST['add'])) {

    include("../../authenticate/config.php");

    $nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
    $genero = filter_var($_POST['genero'], FILTER_SANITIZE_STRING);
    $ano_inicio =  filter_var($_POST['ano_inicio'], FILTER_SANITIZE_STRING);
    $ano_fim =  filter_var($_POST['ano_fim'], FILTER_SANITIZE_STRING);
    $sinopse =  filter_var($_POST['sinopse']);
    $trailer =  filter_var($_POST['trailer'], FILTER_SANITIZE_STRING);
    $writers =  filter_var($_POST['writers'], FILTER_SANITIZE_STRING);
    $director =  filter_var($_POST['director'], FILTER_SANITIZE_STRING);
    $poster_url = filter_var($_POST['poster_url']);
    $banner_url = filter_var($_POST['banner_url']);
    $rating = filter_var($_POST['rating'], FILTER_SANITIZE_STRING);


    //------------------------IMAGEM-------------------------------------------------------------------------------------

    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if ($check !== false) {
        $image = $_FILES["image"]["tmp_name"];
        $imgContent = addslashes(file_get_contents($image));
    } else {
        $erro_imagem = "Porfavor selecione uma imagem para inserir";
    }

    if (empty($poster_url)) {
        $erro_posterURL = "Insira dados no campo nome";
    }

    if (empty($banner_url)) {
        $erro_bannerURL = "Insira dados no campo nome";
    }

    if (empty($rating)) {
        $erro_rating = "Insira dados no campo nome";
    }

    //------------------------Nome-------------------------------------------------------------------------------------

    if (empty($nome)) {
        $erro_nome = "Insira dados no campo nome";
    }

    //------------------------Genero----------------------------------------------------------------------------------

    if (empty($genero)) { //check if string is empty
        $erro_genero = "Insira dados no campo genero";
    }

    //------------------------Anos-------------------------------------------------------------------------------------

    if (empty($ano_inicio)) { //check if string is empty
        $erro_ano_inicio = "Insira dados no campo de ano inicial";
    }

    if (empty($ano_fim)) { //check if string is empty
        $erro_ano_fim = "Insira dados no campo de ano final";
    }

    //------------------------Sinopse-------------------------------------------------------------------------------------

    if (empty($sinopse)) { //check if string is empty
        $erro_sinopse = "Insira dados no campo de sinopse";
    }

    //------------------------Trailer-------------------------------------------------------------------------------------

    if (empty($trailer)) { //check if string is empty
        $erro_trailer = "Insira dados no campo de trailer";
    }

    //------------------------Writers-------------------------------------------------------------------------------------

    if (empty($writers)) { //check if string is empty
        $erro_writers = "Insira dados no campo de writers";
    }

    //------------------------Director-------------------------------------------------------------------------------------

    if (empty($director)) { //check if string is empty
        $erro_director = "Insira dados no campo de diretor";
    }

    //------------------------INSERÇAO----------------------------------------------------------------------------------

    if (($erro_nome == "") && ($erro_genero == "") && ($erro_ano_inicio == "") && ($erro_ano_fim == "") && ($erro_sinopse == "") && ($erro_trailer == "") && ($erro_writers == "") && ($erro_director == "") && ($erro_rating == "") && ($erro_posterURL == "")) {
        $disponivel = 1;

        $insert = $conn->query("INSERT INTO series(poster, nome, genero, ano_inicio, ano_fim, disponivel, synopse, trailer, writers, director, rating, poster_url, banner_url) VALUES ('$imgContent', '$nome', '$genero', '$ano_inicio', '$ano_fim', '$disponivel', '$sinopse', '$trailer', '$writers', '$director','$rating','$poster_url','$banner_url')");
        if ($insert) {
            $conn->close();
            header("Location: series.php");
            die();
        } else {
            echo "File upload failed, please try again";
        }
    } else {
        echo "Oops! Something went wrong";
    }
}
?>

<button type="button" class="collapsible">Add Serie</button>
<div class="content">
    <hr>
    <form role="form" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-2">
                <label>Imagem de Poster (340×503): </label>
            </div>
            <div class="col-md-10">
                <input type="file" name="image" />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Imagem de Banner (1920x1080): </label>
            </div>
            <div class="col-md-10">
                <input type="file" name="banner" />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Poster URL (340×503): </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="poster_url" required />
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Banner URL (1920x1080): </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="banner_url" required />
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Nome: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="nome" required />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Género: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="genero" required />
            </div>
        </div>


        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Ano de início: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="ano_inicio" required />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Ano de término: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="ano_fim" />
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Rating: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="rating" required />
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Sinopse: </label>
            </div>
            <div class="col-md-10">
                <textarea rows="10" style="width:100%" name="sinopse" scols="50" required></textarea>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Trailer: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="trailer" required />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Writers: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="writers" required />
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">
                <label>Director: </label>
            </div>
            <div class="col-md-10">
                <input type="text" style="width:100%" name="director" required />
            </div>
        </div>

        <hr>
        <button class="btn btn-success" style="margin-bottom:20px; float:right;" name="add">Adicionar</button>
    </form>
</div>
<br>

<script>
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
</script>