<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
  <title>PeanutFlix</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../../peanuttime.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="../../index.php">
      <img src="../../assets/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
      PeanutFlix
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filmes
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="../movies.php">All</a>
            <a class="dropdown-item" href="../movies_generos.php">Generos</a>
            <a class="dropdown-item" href="#"></a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Séries
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="../tv.php">All</a>
            <a class="dropdown-item" href="../tv_generos.php">Generos</a>
            <a class="dropdown-item" href="#"></a>
          </div>
        </li>


      </ul>
      <?php
      include("../../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

      if ($autenticado) {
        if ($cargoUtil == "Utilizador") {
      ?>
          <ul class="nav navbar-nav float-md-right">
            <li class="nav-item dropdown ml-auto">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="../../backend/user/perfil.php">Perfil</a>
                <a class="dropdown-item" href="../../backend/user/cli_alugueres.php">Filmes Alugados</a>
                <a class="dropdown-item" style="color:red;" href="../../authenticate/logout.php">Logout</a>
              </div>
            </li>
          </ul>
        <?php
        } elseif ($cargoUtil == "Administrador") {
        ?>
          <ul class="nav navbar-nav float-md-right">
            <li class="nav-item dropdown ml-auto">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="../../backend/admin/perfil.php">Perfil</a>
                <a class="dropdown-item" href="../../backend/admin/filmes.php">Filmes</a>
                <a class="dropdown-item" href="../../backend/admin/alugueres.php">Alugueres</a>
                <a class="dropdown-item" href="../../backend/admin/users.php">Utilizadores</a>
                <a class="dropdown-item" style="color:red;" href="../../authenticate/logout.php">Logout</a>
              </div>
            </li>
          </ul>
        <?php
        }
        ?>
      <?php
      } else {
      ?>
        <ul class="nav navbar-nav float-md-right">
          <li class="nav-item">
            <button type="button" onclick="location.href='../../authenticate/login.php'" class="btn btn-outline-success">Login / Sign In</button>
          </li>
        </ul>
      <?php
      }
      ?>
    </div>
  </nav>


</head>

<body>


  <div class="row" style="padding: 1%">

    <div class="col-md-12">

      <?php
      include("../../authenticate/config.php");

      $genero = ($_GET['gnr']);
      $type = ($_GET["type"]);

      ?>


      <?php if ($type == 'series') {
        echo '<h2>TV ' . $genero . '</h2>';
      } else {
        echo '<h2> ' . $genero . '</h2>';
      }

      if ($autenticado) {

      ?>

        <div class="row" style="padding: 1%">

          <div class="col-md-12">

            <div class="table-responsive">
              <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Poster</th>
                    <th scope="col">Name</th>
                    <th scope="col">Disponibilidade</th>
                    <th scope="col">Genre</th>
                    <th scope="col">Year</th>
                  </tr>
                </thead>

                <tbody>

                  <?php



                  $sql = "SELECT * FROM $type WHERE genero = '$genero'";
                  $result = $conn->query($sql);

                  if ($result->num_rows > 0) {
                    // output data of each row
                    while ($row = $result->fetch_assoc()) {

                      if ($type == 'series') {

                        echo '<tr>
                <td scope="row">' . $row["id"] . '</td>';

                        if ($row["poster"] == NULL) {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="' . $row['poster_url'] . '" style="height: 90px" /></a></td>';
                        } else {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" style="height: 90px" /></a></td>';
                        }
                        echo '
                <td> <a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '">' . $row["nome"] . '</a></td>';

                        if ($row["disponivel"] == 1) {
                          echo '<td><h3 style="color: green">Disponivel</h3></td>';
                        } else {
                          echo '<td><h3 style="color: red">Indisponivel</h3></td>';
                        }

                        echo ' <td><a href = "genresample.php?gnr=' . $row["genero"] . '&type=' . $type . '"> ' . $row["genero"] . ' </a></td>
                <td> ' . $row["ano_inicio"] . ' - ' . $row["ano_fim"] . '</td>
              </tr>';
                      } else {
                        echo '<tr>
                <td scope="row">' . $row["id"] . '</td>';

                        if ($row["poster"] == NULL) {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="' . $row['poster_url'] . '" style="height: 90px" /></a></td>';
                        } else {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" style="height: 90px" /></a></td>';
                        }
                        echo '
                <td> <a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '">' . $row["nome"] . '</a></td>';

                        if ($row["disponivel"] == 1) {
                          echo '<td><h3 style="color: green">Disponivel</h3></td>';
                        } else {
                          echo '<td><h3 style="color: red">Indisponivel</h3></td>';
                        }

                        echo '<td><a href = "genresample.php?gnr=' . $row["genero"] . '&type=' . $type . '"> ' . $row["genero"] . ' </a></td>
                <td> ' . $row["ano"] . '</td>
              </tr>';
                      }
                    }
                  } else {
                    echo "<tr>0 results</tr> ";
                  }
                  ?>

                </tbody>
              </table>
            </div>

          <?php
        } else {
          ?>



            <!-- <img src="https://coverfiles.alphacoders.com/514/51440.jpg" style="width: 100% ; height: 30%"> -->

            <p>

              <div class="table-responsive">

                <table class="table">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Poster</th>
                      <th scope="col">Name</th>
                      <th scope="col">Genre</th>
                      <th scope="col">Year</th>
                    </tr>
                  </thead>

                  <tbody>

                  <?php



                  $sql = "SELECT * FROM $type WHERE genero = '$genero'";
                  $result = $conn->query($sql);

                  if ($result->num_rows > 0) {
                    // output data of each row
                    while ($row = $result->fetch_assoc()) {

                      if ($type == 'series') {

                        echo '<tr>
                <td scope="row">' . $row["id"] . '</td>';

                        if ($row["poster"] == NULL) {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="' . $row['poster_url'] . '" style="height: 90px" /></a></td>';
                        } else {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" style="height: 90px" /></a></td>';
                        }
                        echo '
                <td> <a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '">' . $row["nome"] . '</a></td>
               
                <td><a href = "genresample.php?gnr=' . $row["genero"] . '&type=' . $type . '"> ' . $row["genero"] . ' </a></td>
                <td> ' . $row["ano_inicio"] . ' - ' . $row["ano_fim"] . '</td>
              </tr>';
                      } else {
                        echo '<tr>
                <td scope="row">' . $row["id"] . '</td>';
                        if ($row["poster"] == NULL) {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="' . $row['poster_url'] . '" style="height: 90px" /></a></td>';
                        } else {
                          echo '<td><a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '"><img src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" style="height: 90px" /></a></td>';
                        }
                        echo '
                <td> <a href="../../sample.php?id=' . $row["id"] . '&type=' . $type . '">' . $row["nome"] . '</a></td>
              
                <td><a href = "genresample.php?gnr=' . $row["genero"] . '&type=' . $type . '"> ' . $row["genero"] . ' </a></td>
                <td> ' . $row["ano"] . '</td>
              </tr>';
                      }
                    }
                  } else {
                    echo "<tr>0 results</tr> ";
                  }
                }
                  ?>

                  </tbody>

                </table>

              </div>

          </div>

        </div>










</body>



<footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>

</footer>


</html>