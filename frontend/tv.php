<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>PeanutFlix</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../peanuttime.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php">
            PeanutFlix
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filmes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="movies.php">All</a>
                        <a class="dropdown-item" href="movies_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Séries
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="tv.php">All</a>
                        <a class="dropdown-item" href="tv_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
            </ul>
            <?php
            include("../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio


            if ($autenticado) {
                if ($cargoUtil == "Utilizador") {
            ?>
                    <ul class="nav navbar-nav float-md-right">
                        <li class="nav-item dropdown ml-auto">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../backend/user/perfil.php">Perfil</a>
                                <a class="dropdown-item" href="../backend/user/rent.php">Alugar</a>
                                <a class="dropdown-item" href="../backend/user/cli_alugueres.php">Filmes Alugados</a>
                                <a class="dropdown-item" style="color:red;" href="../authenticate/logout.php">Logout</a>
                            </div>
                        </li>
                    </ul>
                <?php
                } elseif ($cargoUtil == "Administrador") {
                ?>
                    <ul class="nav navbar-nav float-md-right">
                        <li class="nav-item dropdown ml-auto">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../backend/admin/perfil.php">Perfil</a>
                                <a class="dropdown-item" href="../backend/admin/rent.php">Alugar</a>
                                <a class="dropdown-item" href="../backend/admin/filmes.php">Filmes</a>
                                <a class="dropdown-item" href="../backend/admin/series.php">Séries</a>
                                <a class="dropdown-item" href="../backend/admin/atores.php">Atores</a>
                                <a class="dropdown-item" href="../backend/admin/AssociarFilmes.php">Ator-Filmes</a>
                                <a class="dropdown-item" href="../backend/admin/AssociarSeries.php">Ator-Series</a>
                                <a class="dropdown-item" href="../backend/admin/alugueres.php">Alugueres</a>
                                <a class="dropdown-item" href="../backend/admin/users.php">Utilizadores</a>
                                <a class="dropdown-item" style="color:red;" href="../authenticate/logout.php">Logout</a>
                            </div>
                        </li>
                    </ul>
                <?php
                }
                ?>
            <?php
            } else {
            ?>
                <ul class="nav navbar-nav float-md-right">
                    <li class="nav-item">
                        <button type="button" onclick="location.href='../authenticate/login.php'" class="btn btn-outline-success">Login / Sign In</button>
                    </li>
                </ul>
            <?php
            }
            ?>
        </div>
    </nav>


</head>

<body>

    <?php

    include("../authenticate/config.php");

    // $sql = "CREATE TABLE series (
    // id SERIAL PRIMARY KEY,
    // poster VARCHAR(255),
    // banner VARCHAR(255),
    // nome VARCHAR(255) NOT NULL UNIQUE KEY,
    // rating VARCHAR(10),
    // genero VARCHAR(255) NOT NULL,
    // ano INT(4) NOT NULL,
    // disponivel BOOLEAN DEFAULT TRUE,
    // rent_by_user VARCHAR(255) )";
    // if ($conn->query($sql) === TRUE) echo "Tabela series criada com sucesso!!!";
    // else echo "Erro na criação da tabela: " . $conn->error;

    /* INSERT INTO  series(poster,nome,rating, genero, ano) VALUES ('url','nome','rating','genero','ano');


  series :

  INSERT INTO  series(poster,banner,nome,rating, genero, ano) VALUES ('https://vignette.wikia.nocookie.net/the-collectors/images/d/dc/Harry_Potter_and_the_Philosopher%27s_Stone_poster.jpg/revision/latest/scale-to-width-down/340?cb=20121031101002', 'https://www.flixwatch.co/wp-content/uploads/60004478.jpg',"Harry Potter and the Philosopher's Stone",'7.7','fantasy','2001');
  INSERT INTO  series(poster,banner,nome,rating, genero, ano) VALUES ('https://images-na.ssl-images-amazon.com/images/I/514aVStnolL._AC_.jpg','http://assets.fanart.tv/fanart/movies/808/moviebackground/shrek-5316a50c754fb.jpg','Shrek','7.8','animation','2001');
  INSERT INTO  series(poster,banner,nome,rating, genero, ano) VALUES ('https://cdn.seat42f.com/wp-content/uploads/2018/05/01094131/IBIZA-Movie-Poster-Netflix.jpg','https://occ-0-1007-1009.1.nflxso.net/dnm/api/v6/E8vDc_W8CLv7-yMQu8KMEC7Rrr8/AAAABXkDIhPmdMwTrqaLzVa2JDwXMoEVbnFfB23KrPQkXqQzOWV1Wk_15UI9F1cplcYUSIBrSYhTQVv9JEq_Tb8PY6o19a4S.jpg?r=b1d','Ibiza','5.2','comedy-romance','2018');
  INSERT INTO  series(poster,banner,nome,rating, genero, ano) VALUES ('https://m.media-amazon.com/images/M/MV5BMTMxMjY0NzE2M15BMl5BanBnXkFtZTcwNTc3ODcyMw@@._V1_SY1000_CR0,0,677,1000_AL_.jpg','https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/region_US/nbcu-10A17-Full-Image_GalleryCover-en-US-1516995448188._UR1920,1080_RI_.jpg','Shark Tale','6.0','animation','2004');

    */

    if ($autenticado) {

    ?>

        <div class="row" style="padding: 1%">

            <div class="col-md-12">

                <form action="tv.php" method="post" style="margin-bottom: 2%">
                    <input type="text" name="search">
                    <input type="submit" value="Search">
                </form>

                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Poster</th>
                                <th scope="col">Name</th>
                                <th scope="col">Disponibilidade</th>
                                <th scope="col">Genre</th>
                                <th scope="col">Year</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php

                            $search = $_POST["search"];

                            $sql = "SELECT * FROM series WHERE nome LIKE '%$search%'";
                            $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while ($row = $result->fetch_assoc()) {


                                    echo '<td>' . $row["id"] . '</td>';

                                    if ($row["poster"] == NULL) {
                                        echo '<td><a href="sample.php?id=' . $row["id"] . '&type=series"><img src="' . $row["poster_url"] . '" style="height: 90px" /></a></td>';
                                    } else {
                                        echo '<td><a href="sample.php?id=' . $row["id"] . '&type=series"><img src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" style="height: 90px" /></a></td>';
                                    }
                                    echo '
              <td> <a href="sample.php?id=' . $row["id"] . '&type=series">' . $row["nome"] . '</a></td>';

                                    if ($row["disponivel"] == 1) {
                                        echo '<td><h3 style="color: green">Disponivel</h3></td>';
                                    } else {
                                        echo '<td><h3 style="color: red">Indisponivel</h3></td>';
                                    }

                                    echo '<td><a href = "generos/genresample.php?gnr=' . $row["genero"] . '&type=series"> ' . $row["genero"] . ' </a></td>
                <td> ' . $row["ano_inicio"] . ' - ' . $row["ano_fim"] . '</td>
              </tr>';
                                }
                            } else {
                                echo "<tr>0 results</tr>";
                            }
                            ?>

                        </tbody>
                    </table>
                </div>

            <?php
        } else {
            ?>

                <div class="row" style="padding: 1%">

                    <div class="col-md-12">

                        <form action="tv.php" method="post" style="margin-bottom: 2%">
                            <input type="text" name="search">
                            <input type="submit" value="Search">
                        </form>


                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Poster</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Genre</th>
                                        <th scope="col">Year</th>
                                    </tr>
                                </thead>

                                <tbody>


                                <?php

                                $search = $_POST["search"];

                                $sql = "SELECT * FROM series WHERE nome LIKE '%$search%'";
                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while ($row = $result->fetch_assoc()) {


                                        echo '<tr>
<td scope="row">' . $row["id"] . '</td>';
                                        if ($row["poster"] == NULL) {
                                            echo '<td><a href="sample.php?id=' . $row["id"] . '&type=series"><img src="' . $row["poster_url"] . '" style="height: 90px" /></a></td>';
                                        } else {
                                            echo '<td><a href="sample.php?id=' . $row["id"] . '&type=series"><img src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" style="height: 90px" /></a></td>';
                                        }
                                        echo '
<td> <a href="sample.php?id=' . $row["id"] . '&type=series"">' . $row["nome"] . '</a></td>

<td><a href = "generos/genresample.php?gnr=' . $row["genero"] . '&type=series"> ' . $row["genero"] . ' </a></td>
<td> ' . $row["ano_inicio"] . ' - ' . $row["ano_fim"] . '</td>
</tr>';
                                    }
                                } else {
                                    echo "<tr>0 results</tr>";
                                }
                            }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


</body>



<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</footer>


</html>