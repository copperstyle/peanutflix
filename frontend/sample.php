<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>PeanutFlix</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../peanuttime.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php">
            PeanutFlix
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filmes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="movies.php">All</a>
                        <a class="dropdown-item" href="movies_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Séries
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="tv.php">All</a>
                        <a class="dropdown-item" href="tv_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>


            </ul>
            <?php
            include("../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio


            if ($autenticado) {
                if ($cargoUtil == "Utilizador") {
            ?>
                    <ul style="margin-top: 0.5%; float: right;">
                        <li class="nav-item dropdown ml-auto">
                            <a class="nav-link dropdown-toggle" href="#" style="color: grey !important;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../backend/user/perfil.php">Perfil</a>
                                <a class="dropdown-item" href="../backend/user/rent.php">Alugar</a>
                                <a class="dropdown-item" href="../backend/user/cli_alugueres.php">Filmes Alugados</a>
                                <a class="dropdown-item" style="color:red;" href="../authenticate/logout.php">Logout</a>
                            </div>
                        </li>
                    </ul>
                <?php
                } elseif ($cargoUtil == "Administrador") {
                ?>
                    <ul style="margin-top: 0.5%; float: right;">
                        <li class="nav-item dropdown ml-auto">
                            <a class="nav-link dropdown-toggle" href="#" style="color: grey !important;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../backend/admin/perfil.php">Perfil</a>
                                <a class="dropdown-item" href="../backend/admin/rent.php">Alugar</a>
                                <a class="dropdown-item" href="../backend/admin/filmes.php">Filmes</a>
                                <a class="dropdown-item" href="../backend/admin/series.php">Séries</a>
                                <a class="dropdown-item" href="../backend/admin/atores.php">Atores</a>
                                <a class="dropdown-item" href="../backend/admin/AssociarFilmes.php">Ator-Filmes</a>
                                <a class="dropdown-item" href="../backend/admin/AssociarSeries.php">Ator-Series</a>
                                <a class="dropdown-item" href="../backend/admin/alugueres.php">Alugueres</a>
                                <a class="dropdown-item" href="../backend/admin/users.php">Utilizadores</a>
                                <a class="dropdown-item" style="color:red;" href="../authenticate/logout.php">Logout</a>
                            </div>
                        </li>
                    </ul>
                <?php
                }
                ?>
            <?php
            } else {
            ?>
                <ul class="nav navbar-nav float-md-right">
                    <li class="nav-item">
                        <button type="button" onclick="location.href='../authenticate/login.php'" class="btn btn-outline-success">Login / Sign In</button>
                    </li>
                </ul>
            <?php
            }
            ?>
        </div>
    </nav>


</head>

<body>


    <div class="row" style="padding:1%">

        <?php

        include("../authenticate/config.php");

        $id = ($_GET['id']);
        $type = ($_GET["type"]);

        $sql = "SELECT * FROM $type WHERE id=$id";
        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc()) {

            if ($row["poster"] == NULL) {

                echo '
                <div class="col-md-2">
                      <img src="' . $row["poster_url"] . '" " />';
            } else {
                echo '
                <div class="col-md-2">
                      <img src="data:image/jpeg;base64,' . base64_encode($row['poster']) . '" " />';
            }
            echo '<h2 style="padding: 1%">Synopse :</h2>
        <p>' . $row["synopse"] . '</p>
    </div>

    <div class="col-md-5">
    <h1>' . $row["nome"] . '</h1>
    <div class="row">

        <div class="col-md-6">
            <h2><span class="fa fa-star checked"></span> ' . $row["rating"] . '</h2>
            

        </div>
        <div class="col-md-6">';

            if ($autenticado) {

                if ($row["disponivel"] = 1) {

        ?>
                    <!-- <button class="button" name="rent" style="color: black">RENT</button> -->

            <?php
                } else {
                    echo '<h2 style="color: red">Indisponivel</h2>';
                }
            } else {
            }

            echo '</div>
    </div>
    <p>
        <h4>Director: </h4>' . $row["director"] . '
    </p>
    <p>
        <h4>Writer: </h4>' . $row["writers"] . '
    </p>
</div>
<div class=" col-md-5"> 
    <iframe width="100%" height="400" src="' . $row["trailer"] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <h1>Stars</h1>';

            ?>



            <div class="tableFixHead">
                <table class="table  table-hover">
                    <thead class="thead-dark">
                        <tr>

                            <th scope="col">Foto</th>
                            <th scope="col">Name</th>
                            <th scope="col">Character</th>

                        </tr>
                    </thead>
                    <tbody>

                    <?php

                    if ($type == 'filmes') {

                        $sql = "SELECT * FROM atores INNER JOIN atores_filmes USING(ator_id) WHERE  atores_filmes.film_id = $id";
                        $result = $conn->query($sql);

                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {

                                if ($row["foto"] == NULL) {
                                    echo '<tr>
                                    <th><img src="' . $row["foto_url"] . '" style="height: 110px" /></th>';
                                } else {

                                    echo '<tr>
                                    <th><img src="data:image/jpeg;base64,' . base64_encode($row['foto']) . '" style="height: 110px" /></th>';
                                }

                                echo '
                            <td><a>' . $row["nome"] . ' </a></td>
                            <td> ' . $row["personagem"] . ' </td>
                        </tr>';
                            }
                        } else {
                            echo "<tr>0 results</tr>";
                        }
                    } else {

                        $sql = "SELECT * FROM atores INNER JOIN atores_series USING(ator_id) WHERE  atores_series.serie_id = $id";
                        $result = $conn->query($sql);

                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {


                                if ($row["foto"] == NULL) {
                                    echo '<tr>
                                    <th><img src="' . $row["foto_url"] . '" style="height: 110px" /></th>';
                                } else {

                                    echo '<tr>
                                    <th><img src="data:image/jpeg;base64,' . base64_encode($row['foto']) . '" style="height: 110px" /></th>';
                                }

                                echo '
                            <td><a>' . $row["nome"] . ' </a></td>
                            <td> ' . $row["personagem"] . ' </td>
                        </tr>';
                            }
                        } else {
                            echo "<tr>0 results</tr>";
                        }
                    }
                }


                    ?>

            </div>
    </div>



</body>

<footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</footer>

</html>