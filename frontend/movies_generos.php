<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/docs/4.0/../assets/img/favicons/favicon.ico">
    <title>PeanutFlix</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../peanuttime.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="../index.php">
            PeanutFlix
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filmes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="movies.php">All</a>
                        <a class="dropdown-item" href="movies_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="frontend/movies.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Séries
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="tv.php">All</a>
                        <a class="dropdown-item" href="tv_generos.php">Generos</a>
                        <a class="dropdown-item" href="#"></a>
                    </div>
                </li>


            </ul>
            <?php
            include("../authenticate/verifica.php"); //em todas as páginas que queiram verificar a autenticão colocar esta linha no inicio

            if ($autenticado) {
                if ($cargoUtil == "Utilizador") {
            ?>
                    <ul style="margin-top: 0.5%; float: right;">
                        <li class="nav-item dropdown ml-auto">
                            <a class="nav-link dropdown-toggle" href="#" style="color: grey !important;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../backend/user/perfil.php">Perfil</a>
                                <a class="dropdown-item" href="../backend/user/rent.php">Alugar</a>
                                <a class="dropdown-item" href="../backend/user/cli_alugueres.php">Filmes Alugados</a>
                                <a class="dropdown-item" style="color:red;" href="../authenticate/logout.php">Logout</a>
                            </div>
                        </li>
                    </ul>
                <?php
                } elseif ($cargoUtil == "Administrador") {
                ?>
                    <ul style="margin-top: 0.5%; float: right;">
                        <li class="nav-item dropdown ml-auto">
                            <a class="nav-link dropdown-toggle" href="#" style="color: grey !important;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <?php echo $nomeUtil ?> </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="../backend/admin/perfil.php">Perfil</a>
                                <a class="dropdown-item" href="../backend/admin/rent.php">Alugar</a>
                                <a class="dropdown-item" href="../backend/admin/filmes.php">Filmes</a>
                                <a class="dropdown-item" href="../backend/admin/series.php">Séries</a>
                                <a class="dropdown-item" href="../backend/admin/atores.php">Atores</a>
                                <a class="dropdown-item" href="../backend/admin/AssociarFilmes.php">Ator-Filmes</a>
                                <a class="dropdown-item" href="../backend/admin/AssociarSeries.php">Ator-Series</a>
                                <a class="dropdown-item" href="../backend/admin/alugueres.php">Alugueres</a>
                                <a class="dropdown-item" href="../backend/admin/users.php">Utilizadores</a>
                                <a class="dropdown-item" style="color:red;" href="../authenticate/logout.php">Logout</a>
                            </div>
                        </li>
                    </ul>
                <?php
                }
                ?>
            <?php
            } else {
            ?>
                <ul class="nav navbar-nav float-md-right">
                    <li class="nav-item">
                        <button type="button" onclick="location.href='../authenticate/login.php'" class="btn btn-outline-success">Login / Sign In</button>
                    </li>
                </ul>
            <?php
            }
            ?>
        </div>
    </nav>


</head>

<body>

    <div class="row" style="padding-top: 1%">

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Comedy&type=filmes"><img src="../assets/img/movies/comedy2.png" style="width: 100%" /></a>

                <h3><span> <strong>Comedy</strong></span></h3>

            </div>

        </div>
        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Sci-Fi&type=filmes"><img src="../assets/img/movies/scify2.png" style="width: 100%" /></a>

                <h3><span> <strong>Sci-Fi</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Horror&type=filmes"><img src="../assets/img/movies/horror.png" style="width: 100%" /></a>

                <h3><span> <strong>Horror</strong></span></h3>
            </div>

        </div>


    </div>

    <div class="row" style="padding-top: 1%">

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Romance&type=filmes"><img src="../assets/img/movies/romance.png" style="width: 100%" /></a>

                <h3><span> <strong>Romance</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Action&type=filmes"><img src="../assets/img/movies/action.png" style="width: 100%" /></a>

                <h3><span> <strong>Action</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Thriller&type=filmes"><img src="../assets/img/movies/thriller.png" style="width: 100%" /></a>

                <h3><span> <strong>Thriller</strong></span></h3>
            </div>

        </div>

    </div>


    <div class="row" style="padding-top: 1%">

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Drama&type=filmes"><img src="../assets/img/movies/drama.png" style="width: 100%" /></a>

                <h3><span> <strong>Drama</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Mystery&type=filmes"><img src="../assets/img/movies/mystery.png" style="width: 100%" /></a>

                <h3><span> <strong>Mystery</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Crime&type=filmes"><img src="../assets/img/movies/crime.png" style="width: 100%" /></a>

                <h3><span> <strong>Crime</strong></span></h3>
            </div>

        </div>

    </div>

    <div class="row" style="padding-top: 1%">

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Animation&type=filmes"><img src="../assets/img/movies/animation.png" style="width: 100%" /></a>

                <h3><span> <strong>Animation</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Adventure&type=filmes"><img src="../assets/img/movies/adventure.png" style="width: 100%" /></a>

                <h3><span> <strong>Adventure</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Fantasy&type=filmes"><img src="../assets/img/movies/fantasy.png" style="width: 100%" /></a>

                <h3><span> <strong>Fantasy</strong></span></h3>
            </div>

        </div>

    </div>

    <div class="row" style="padding-top: 1%">

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Comedy-Romance&type=filmes"><img src="../assets/img/movies/romance-comedy.png" style="width: 100%" /></a>

                <h3><span> <strong>Comedy-Romance</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr=Action-Comedy&type=filmes"><img src="../assets/img/movies/action-comedy.png" style="width: 100%" /></a>

                <h3><span> <strong>Action-Comedy</strong></span></h3>
            </div>

        </div>

        <div class="col-md-4">
            <div class="img-thumbnail">
                <a href="generos/genresample.php?gnr='SuperHeroes'&type=filmes"><img src="../assets/img/movies/superheroes.png" style="width: 100%" /></a>

                <h3><span> <strong>SuperHeroes</strong></span></h3>
            </div>

        </div>

    </div>

    </div>

    </div>

    </div>





    <div class="col-md-1"></div>



</body>



<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</footer>


</html>